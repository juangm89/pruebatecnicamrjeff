package es.juan.pruebatecnica.controller;

import es.juan.pruebatecnica.controller.VO.CalcularPrecio;
import es.juan.pruebatecnica.controller.VO.Producto;
import es.juan.pruebatecnica.entity.Cupon;
import es.juan.pruebatecnica.repository.CuponRepository;
import es.juan.pruebatecnica.service.CuponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by juan on 22/07/17.
 */
@RestController
public class CuponController {

    @Autowired
    private CuponService cuponService;

    @RequestMapping(value = "/api/cupones/calcularPrecio", method = RequestMethod.POST)
    public BigDecimal calcularPrecio(@Valid @RequestBody CalcularPrecio calcularPrecio) {
        return cuponService.obtenerPrecioPedido(calcularPrecio.getCodigoCupon(), calcularPrecio.getProductos());
    }

}
