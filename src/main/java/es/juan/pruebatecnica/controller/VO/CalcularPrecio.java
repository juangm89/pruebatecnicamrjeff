package es.juan.pruebatecnica.controller.VO;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by juan on 24/07/17.
 */
public class CalcularPrecio {

    @NotBlank
    private String codigoCupon;

    @Valid
    private List<Producto> productos;

    public CalcularPrecio(String codigoCupon, List<Producto> productos) {
        this.codigoCupon = codigoCupon;
        this.productos = productos;
    }

    public CalcularPrecio() {
    }

    public String getCodigoCupon() {
        return codigoCupon;
    }

    public void setCodigoCupon(String codigoCupon) {
        this.codigoCupon = codigoCupon;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
}
