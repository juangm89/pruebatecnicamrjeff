package es.juan.pruebatecnica.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by juan on 22/07/17.
 */
@Entity
@Table(name = "cupon")
public class Cupon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "codigo", unique = true)
    private String codigo;
    @Column(name = "valor")
    private BigDecimal valor;

    public Cupon(Long id, String codigo, BigDecimal valor) {
        this.id = id;
        this.codigo = codigo;
        this.valor = valor;
    }

    public Cupon() {
    }

    public Long getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
