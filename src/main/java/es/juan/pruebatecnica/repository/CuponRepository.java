package es.juan.pruebatecnica.repository;

import es.juan.pruebatecnica.entity.Cupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by juan on 22/07/17.
 */
public interface CuponRepository extends JpaRepository<Cupon, String> {

    Cupon findByCodigo(String codigo);
}