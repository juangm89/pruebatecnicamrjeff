package es.juan.pruebatecnica.service.Impl;

import es.juan.pruebatecnica.controller.VO.Producto;
import es.juan.pruebatecnica.entity.Cupon;
import es.juan.pruebatecnica.repository.CuponRepository;
import es.juan.pruebatecnica.service.CuponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by juan on 22/07/17.
 */
@Service
public class CuponServiceImpl implements CuponService{

    @Autowired
    private CuponRepository repository;

    @Override
    public BigDecimal obtenerPrecioPedido(String codigoCupon, List<Producto> productos) {
        // Declaración de variables
        BigDecimal total = new BigDecimal(0);
        // Calculamos el total si existen productos
        if (productos != null){
            for (Producto producto : productos) {
                total = total.add(producto.getPrecio());
            }
        }

        // Recuperamos el cupon
        Cupon cupon = repository.findByCodigo(codigoCupon);
        // Aplicamos el cupon si existe
        if (cupon != null) {
            total = total.subtract(cupon.getValor());
        }

        return total;
    }
}
