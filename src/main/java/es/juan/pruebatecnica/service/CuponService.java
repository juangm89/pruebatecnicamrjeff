package es.juan.pruebatecnica.service;

import es.juan.pruebatecnica.controller.VO.Producto;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by juan on 22/07/17.
 */
public interface CuponService {

    BigDecimal obtenerPrecioPedido(String codigoCupon, List<Producto> productos);
}
