package es.juan.pruebatecnica.repository;

import es.juan.pruebatecnica.entity.Cupon;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by juan on 24/07/17.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CuponRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CuponRepository cuponRepository;

    @Ignore
    @Test
    public void findByCodigo_recuperaCupon() {
        // Insertamos
        Cupon cupon = new Cupon(5l, "TEST", new BigDecimal(10));
        entityManager.persist(cupon);
        entityManager.flush();

        // Buscamos
        Cupon cuponRecuperado = cuponRepository.findByCodigo(cupon.getCodigo());

        // Comprobamos
        assert(cuponRecuperado.getCodigo())
                .equals(cupon.getCodigo());
    }
}
