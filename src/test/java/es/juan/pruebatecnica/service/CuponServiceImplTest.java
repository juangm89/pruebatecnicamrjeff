package es.juan.pruebatecnica.service;

import es.juan.pruebatecnica.controller.VO.Producto;
import es.juan.pruebatecnica.entity.Cupon;
import es.juan.pruebatecnica.repository.CuponRepository;
import es.juan.pruebatecnica.service.Impl.CuponServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by juan on 24/07/17.
 */
@RunWith(SpringRunner.class)
public class CuponServiceImplTest {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public CuponService employeeService() {
            return new CuponServiceImpl();
        }
    }

    @Autowired
    private CuponService cuponService;

    @MockBean
    private CuponRepository cuponRepository;

    @Before
    public void setUp() {
        Cupon test = new Cupon(1l, "TEST", new BigDecimal(10));

        Mockito.when(cuponRepository.findByCodigo(test.getCodigo()))
                .thenReturn(test);
    }

    @Test
    public void codigoValido_encuentraCupon() {
        String codigoCupon = "TEST";
        List<Producto> productos = new ArrayList<Producto>();
        productos.add(new Producto("Camisa", new BigDecimal(3)));
        productos.add(new Producto("Traje", new BigDecimal(12)));
        BigDecimal precio = cuponService.obtenerPrecioPedido(codigoCupon, productos);

        assert(precio)
                .equals(new BigDecimal(5));
    }

    @Test
    public void codigoNoValido_noEncuentraCupon() {
        String codigoCupon = "TEST2";
        List<Producto> productos = new ArrayList<Producto>();
        productos.add(new Producto("Camisa", new BigDecimal(3)));
        productos.add(new Producto("Traje", new BigDecimal(12)));
        BigDecimal precio = cuponService.obtenerPrecioPedido(codigoCupon, productos);

        assert(precio)
                .equals(new BigDecimal(15));
    }
}