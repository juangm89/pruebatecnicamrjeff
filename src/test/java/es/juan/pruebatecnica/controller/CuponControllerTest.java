package es.juan.pruebatecnica.controller;

import es.juan.pruebatecnica.MrJeffApplication;
import es.juan.pruebatecnica.controller.VO.CalcularPrecio;
import es.juan.pruebatecnica.controller.VO.Producto;
import es.juan.pruebatecnica.service.CuponService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.when;
import static org.springframework.http.converter.json.Jackson2ObjectMapperBuilder.json;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by juan on 24/07/17.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CuponController.class)
@WebAppConfiguration
public class CuponControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CuponService service;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Ignore
    @Test
    public void calcularPrecionReturnOk() throws Exception {
        // Creamos el objeto de la llamada
        CalcularPrecio calcularPrecio = new CalcularPrecio();
        calcularPrecio.setCodigoCupon("TEST");
        List<Producto> productos = new ArrayList<Producto>();
        productos.add(new Producto("Camisa", new BigDecimal(3)));
        productos.add(new Producto("Traje", new BigDecimal(12)));
        calcularPrecio.setProductos(productos);

        when(service.obtenerPrecioPedido(calcularPrecio.getCodigoCupon(), calcularPrecio.getProductos()))
                .thenReturn(new BigDecimal(10));
        this.mockMvc.perform(post("/api/cupones/calcularPrecio")
                .contentType(MediaType.APPLICATION_JSON).content(this.json(calcularPrecio)))
                .andExpect(status().isOk())
                .andExpect(content().string(contains("10")));
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
